import { Component } from '@angular/core';
import { PdfService } from './pdf.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pdf-service';
  constructor(private pdfService: PdfService) {}

  downloadPdf() {
    this.pdfService.generatePdf();
  }
}
